package com.br.tcc.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.br.tcc.model.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, String> {

	public Optional<Cliente> findByEmailAndSenha(String email, String senha);

}