package com.br.tcc.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.tcc.model.Cliente;
import com.br.tcc.model.Login;
import com.br.tcc.repository.ClienteRepository;

@RestController
@CrossOrigin(origins="*")
public class ClienteController {

	@Autowired
	ClienteRepository clienteRepository;

	@RequestMapping(method = RequestMethod.POST, path = "/cliente")
	public Cliente criarCliente(@Valid @RequestBody Cliente cliente) {

		return clienteRepository.save(cliente);

	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/excluir")
	public Cliente excluirCliente(@Valid @RequestBody Cliente cliente) {

		clienteRepository.delete(cliente);
		return cliente;

	}

	@RequestMapping(method = RequestMethod.POST, path = "/login")
	public ResponseEntity<?> fazerLogin(@RequestBody Login login) {

		Optional<Cliente> clienteBancoOptional = clienteRepository.findByEmailAndSenha(login.getEmail(),
				login.getSenha());

		if (!clienteBancoOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		return new ResponseEntity<Cliente>(clienteBancoOptional.get(), HttpStatus.OK);
	}

}
